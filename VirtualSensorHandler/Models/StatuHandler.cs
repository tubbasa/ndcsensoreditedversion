﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExagateVirtualSensors.Models
{
    public class StatuHandler
    {
        public List<Error> handler{ get; set; }
        public int totalErrorCount { get; set; }
        public List<string> successedPaths { get; set; }
        public List<string> erroredPaths { get; set; }
    }
}
