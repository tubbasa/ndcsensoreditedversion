﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExagateVirtualSensors.Models
{
    public class Error
    {
        public string fileName { get; set; }
        public string methodName { get; set; }
        public string error { get; set; }
        public ErrorStatusCode code { get; set; }
        public ErrorTypes typeOfError { get; set; }

    }
}
