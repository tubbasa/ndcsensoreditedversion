﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExagateVirtualSensors
{
    public class VirtualSensor
    {
        public string SensorName { get; set; }
        public int LocationId { get; set; }
        public double Value { get; set; }
    }
}
