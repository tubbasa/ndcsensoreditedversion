﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExagateVirtualSensors
{
    public interface ILogger
    {
        void Log(string methodname, string message);
        void Log(string methodname, Exception ex, string message = "");
    }
}
