﻿using ExagateVirtualSensors.Models;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ExagateVirtualSensors
{
    public class VirtualSensorHandler : ILogger
    {
        public string Host { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string RemoteDirectory { get; private set; }
        public string LocalDirectory { get; private set; }
        public string TempDirectory { get; private set; }
        public string[] FileTypes { get; private set; }
        public string SensorLocationFilePath { get; set; }
        public List<KeyValuePair<string, int>> LocationsList { get; set; }
        private List<bool> validationStatusList = new List<bool>();
        public List<string> message = new List<string>();
        public List<KeyValuePair<DateTime, string>> PathDateList { get; set; }
        public static StatuHandler statuhandler = new StatuHandler { handler = new List<Error>(), totalErrorCount = 0, erroredPaths = new List<string>(), successedPaths = new List<string>() };



        private static StatuHandler Statuhandler
        {
            get
            {
                return statuhandler;
            }

        }


        public VirtualSensorHandler()
        { }
        //private string TempDirectory = "d://tempDirectory";

        private static EventLog _Logger;

        public static EventLog Logger
        {
            get
            {
                if (_Logger == null)
                    _Logger = new EventLog();
                return _Logger;
            }

            set
            {
                Logger = value;
            }
        }

        public enum Alarms
        {
            Data = 0,
            DateTime = 1,
            FolderStatu = 2
        }

        /// <summary>
        /// sFTP bağlantısı için gerekli bilgiler girilmelidir.
        /// </summary>
        /// <param name="host">sFTP ip adresi örn: sftp.domain.com</param>
        /// <param name="username">sFTP kullanıcı adı</param>
        /// <param name="password">sFTP şifresi</param>
        /// <param name="remotedirectory">Dosyaların çekileceği sFTP dizini. Örn: /root/directory/sensors</param>
        /// <param name="localdirectory">Çekilen dosyaların download edileceği local dizin</param>


        public VirtualSensorHandler(string host, string username, string password, string remotedirectory, string localdirectory, string[] filetypes, string sensorlocationfilepath, string TempDirectory)
        {
            try
            {
                this.Host = host;
                this.UserName = username;
                this.Password = password;
                this.RemoteDirectory = remotedirectory;
                this.LocalDirectory = localdirectory;
                this.FileTypes = filetypes;
                this.SensorLocationFilePath = sensorlocationfilepath;
                this.TempDirectory = TempDirectory;
                LocationsList = new List<KeyValuePair<string, int>>();
                LocationsList.AddRange(GetLocations());

            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
        }

        /// <summary>
        /// Dosyaları çekmeden önce dizini temizle
        /// </summary>
        private void EmptyLocalDirectory()
        {
            try
            {
                foreach (var f in Directory.GetFiles(@LocalDirectory))
                    File.Delete(f);
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
                throw;
            }
        }

        /// <summary>
        /// Contractor'da verilen dizindeki tüm dosyaları çekip local dizine indirir.
        /// </summary>
        private void DownloadAllFiles()
        {
            try
            {
                using (var sftp = new SftpClient(Host, UserName, Password))
                {
                    sftp.Connect();
                    var files = sftp.ListDirectory(@RemoteDirectory);

                    foreach (var file in files)
                    {
                        if (!file.IsDirectory && !file.IsSymbolicLink)
                        {
                            string localfilename = Path.Combine(LocalDirectory, Path.GetFileName(file.Name));
                            string remotefilename = Path.Combine(RemoteDirectory, Path.GetFileName(file.Name)).Replace("\\", "//");
                            using (Stream stream = File.OpenWrite(localfilename))
                            {
                                sftp.DownloadFile(remotefilename, stream);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                throw;
            }
        }

        /// <summary>
        /// İlgili dizindeki istenilen dosya tipindeki dosya yollarını döndürür.
        /// </summary>
        /// <param name="directorypath">Dizin yolu</param>
        /// <param name="filetypes">İstenilen dosya tipleri. Örn: txt,csv,xlsx</param>
        /// <returns></returns>
        private IEnumerable<string> GetAllFiles()
        {
            List<string> fileList = new List<string>();
            try
            {
                foreach (var t in FileTypes)
                {
                    fileList.AddRange(Directory.GetFiles(LocalDirectory, "*." + t, SearchOption.AllDirectories));
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
                throw;
            }
            return fileList;
        }

        /// <summary>
        /// Verilen dosya yolundaki her bir satırı VirtualSensor tipinde döndürür.
        /// </summary>
        private IEnumerable<VirtualSensor> GetSensorValues(string sensorfilepath)
        {
            var sensorsList = new List<VirtualSensor>();

            try
            {
                //DBPHR04
                foreach (var line in File.ReadAllLines(sensorfilepath))
                {
                    var splitline = line.Trim().Split(','); //satir Sensoradi,Value şeklinde
                    if (IsValidSensorDataPair(splitline))
                    {
                        int locationID = GetLocationId(splitline[0]);
                        if (IsValidLocaiton(locationID))
                        {
                            sensorsList.Add(new VirtualSensor
                            {
                                SensorName = splitline[0],
                                Value = Convert.ToDouble(splitline[1]),
                                LocationId = locationID
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });

                /*
                 * GetSensorsWithValues() metodu içinde kullanılan tüm metotların catch'inde logdan sonra throw ettim.
                 * Çünkü örneğin dosyaları indirirken bir hata oluştuysa alt satırların işleme devam etmesinin bir an-
                 * lamı yok. Fakat bu metoda ftp içinden çekilen dosyaların her biri sırayla verildiği için biri catche
                 * düşse diğeri düşmeyebilir. Bu nedenle burdaki hatayı sadece logluyorum throw etmiyorum.
                 *
                 */
            }

            return sensorsList;
        }

        /// <summary>
        /// sFTP'den çekilen tüm dosyalardaki tüm sensler verilerini getirir.
        /// </summary>
        /// <returns>Bir string, double KeyValuePair IEnumerable'ı olarak döner.</returns>
        public IEnumerable<VirtualSensor> GetSensorsWithValues()
        {
            var allSensorValues = new List<VirtualSensor>();
            try
            {
                EmptyLocalDirectory();
                DownloadAllFiles();
                var files = GetAllFiles();
                var locations = GetLocations();

                foreach (var f in files)
                {
                    var name = f.Split('\\');
                    if (!statuhandler.erroredPaths.Contains(name[name.Length - 1]))
                    {
                        allSensorValues.AddRange(GetSensorValues(f));
                    }

                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex, "Exception nedeniyle proses çalışamadı. Bir saat sonra tekrar denenecek.");
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }

            return allSensorValues;
        }

        /// <summary>
        /// SensörAdi, locationID formatında verilen dosyayı parse ederek lokasyonları döndürür.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<string, int>> GetLocations()
        {
            var sensorLocationPairList = new List<KeyValuePair<string, int>>();
            try
            {
                using (var reader = new StreamReader(SensorLocationFilePath))
                {
                    string line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        var splittedline = line.Trim().Split(';');
                        if (IsValidSensorLocaitonPair(splittedline))
                        {
                            var sensorlocationpair = new KeyValuePair<string, int>(splittedline[0], Convert.ToInt32(splittedline[1]));
                            sensorLocationPairList.Add(sensorlocationpair);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
                //throw;
            }

            return sensorLocationPairList;
        }

        /// <summary>
        /// Verilen satır datasının SensorName, 25.6 gibi doğru formattta olup olmadığını kontrol eder.
        /// </summary>
        private bool IsValidSensorDataPair(string[] linedata)
        {
            if (linedata.Length == 2)
            {
                if ((!string.IsNullOrEmpty(linedata[0]) || !string.IsNullOrWhiteSpace(linedata[0]))
                    && double.TryParse(linedata[1], out double result))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Verilen satır datasının SensorName, 1116 gibi doğru formattta olup olmadığını kontrol eder.
        /// </summary>
        private bool IsValidSensorLocaitonPair(string[] linedata)
        {
            if (linedata.Length == 2)
            {
                if ((!string.IsNullOrEmpty(linedata[0]) || !string.IsNullOrWhiteSpace(linedata[0]))
                    && int.TryParse(linedata[1], out int result))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }

        /// <summary>
        /// Verilen sensör adınına göre lokacationId döndürür.
        /// </summary>
        private int GetLocationId(string sensorname)
        {
            int locationID = 0;
            try
            {
                locationID = LocationsList.FirstOrDefault(x => x.Key == sensorname).Value;
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
            }
            return locationID;
        }

        /// <summary>
        /// LoactionID'nin gerçek bir locationID olup olmadığını kontrol eder.
        /// </summary>
        private bool IsValidLocaiton(int locationID)
        {
            return locationID > 0 ? true : false;
        }

        /// <summary>
        /// Log metodu. Yapılmak istenilen bilgilendirmeler buradan yapılır. Loglama Warning ya da Error değil Info seviyesindedir.
        /// </summary>
        public void Log(string methodname, string message)
        {
            Logger.Source = "NDCVirtualSensorService";
            Logger.WriteEntry("MethodName: " + methodname + Environment.NewLine
                + "Message: " + message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Log metodu. Kodda meydana gelen exceptionlar burada loglanır. Loglama Error seviyesindedir.
        /// </summary>
        public void Log(string methodname, Exception ex, string message = "")
        {
            var st = new StackTrace(ex, true);
            var frame = st.GetFrame(st.FrameCount - 1);
            var linenumber = frame.GetFileLineNumber();
            Logger.Source = "NDCVirtualSensorService";
            Logger.WriteEntry("MethodName: " + methodname + " LineNumber: " + linenumber.ToString() + Environment.NewLine
                + "Exception: -> " + ex.Message + Environment.NewLine
                + "InnerExc.: -> " + (ex.InnerException ?? new Exception()).Message + Environment.NewLine
                + "Message:   -> " + message
                , EventLogEntryType.Error);
        }

        //Alarm Kontrolleri ile ilgili işlemlerin yapıldığı kısım
        public StatuHandler HasNoAlarm()
        {
            var isDataFail = IfNotExistAnyAlarm(Alarms.Data);
            var isDateTimeFail = IfNotExistAnyAlarm(Alarms.DateTime);
            var isFolderIsEmpty = IfNotExistAnyAlarm(Alarms.FolderStatu);
            //(isDataFail && isDateTimeFail && isFolderIsEmpty) ? true : false
            return statuhandler;
        }

        public bool IfNotExistAnyAlarm(Alarms Control)
        {
            bool result = true;
            if (!Directory.Exists(TempDirectory))
                Directory.CreateDirectory(TempDirectory);
            else
                EmptyTempControlDirectory();
            DownloadAllControlFiles();
            switch (Control)
            {
                case Alarms.Data:
                    result = DataAlarmControl();
                    break;

                case Alarms.DateTime:
                    result = DateTimeAlarmControl();
                    break;
                case Alarms.FolderStatu:
                    result = FolderAlarmControl();
                    break;

                default:
                    break;
            }
            return result;
        }

        private IEnumerable<string> GetAllTempFiles()
        {
            List<string> fileList = new List<string>();
            try
            {
                foreach (var t in FileTypes)
                {
                    fileList.AddRange(Directory.GetFiles(TempDirectory, "*." + t, SearchOption.AllDirectories));
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                throw;
            }
            return fileList;
        }

        public bool DateTimeAlarmControl()
        {
            bool isValid = true;
            try
            {
                foreach (var item in PathDateList)
                {
                    var theDate = new DateTime(item.Key.Year, item.Key.Month, item.Key.Day, item.Key.Hour, item.Key.Minute, item.Key.Second);
                    var theDate2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 0, 0);
                    if (item.Key.Date != DateTime.Now.Date)
                    {
                        Log(MethodBase.GetCurrentMethod().Name, DateTime.Now.Date + " tarihinde herhangi" +
                                                                                    "bir dosya yazımı gerçekleşmemiştir. [bu güne ait herhangi bir yeni dosya bulunmamakta.]");
                        var errorStr = item.Value + " dosyasının tarih kısmında hata bulunmaktadır. [Okunmayacak] ";
                        Error detectedErrror = new Error { error = errorStr, fileName = item.Value, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error };
                        if (!statuhandler.handler.Contains(detectedErrror))
                        {
                            statuhandler.handler.Add(new Error { error = errorStr, fileName = item.Value, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error });
                            statuhandler.erroredPaths.Add(item.Value);
                        }

                        //isValid = false;
                    }
                    else
                    {
                        if (!theDate.Hour.Equals(theDate2.Hour))
                        {
                            Log(MethodBase.GetCurrentMethod().Name, DateTime.Now.TimeOfDay + " Saatinde herhangi" +
                                                                                     "bir dosya yazımı gerçekleşmemiştir.");
                            var errorStr = item.Value + " dosyasının tarih bilgisi doğru ancak saat bilgisinde hata bulunmaktadır. [Okunmayacak] ";
                            Error detectedError = new Error { error = errorStr, fileName = item.Value, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error };
                            if (!statuhandler.handler.Contains(detectedError))
                            {
                                statuhandler.handler.Add(detectedError);
                                statuhandler.erroredPaths.Add(item.Value);
                            }

                            //isValid = false;
                        }
                        else
                        {
                            var errorStr = item.Value + " dosyasının tarih ve saat bilgisi doğru okunmak için sıraya alındı.";
                            Error detectedError = new Error { error = errorStr, fileName = item.Value, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error };
                            if (!statuhandler.handler.Contains(detectedError))
                            {
                                statuhandler.handler.Add(detectedError);
                                statuhandler.successedPaths.Add(item.Value);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isValid = false;
                Log(MethodBase.GetCurrentMethod().Name, ex, "Exception nedeniyle proses çalışamadı. Bir saat sonra tekrar denenecek.");
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
            return isValid;
        }

        public bool DataAlarmControl()
        {
            bool isValid = true;
            try
            {
                var files = GetAllTempFiles();
                foreach (var f in files)
                {
                    ControlToValue(f);
                }
                var counter = 0;
                foreach (var item in validationStatusList)
                {
                    if (item == false)
                    {
                        var fileName = files.ToArray()[counter].Split('\\');
                        //var errorStr = fileName + " dosyasının tarih kısmında hata bulunmaktadır.";
                        //statuhandler.handler.Add(new Error { error = errorStr, fileName = fileName[fileName.Length - 1], typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error });
                        statuhandler.erroredPaths.Add(files.ToArray()[counter]);

                    }
                    else
                    {
                        //var fileName = files.ToArray()[counter].Split('\\');
                        //statuhandler.handler.Add(new Error { error = string.Empty, fileName = fileName[fileName.Length-1], typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Success });
                    }

                    counter++;

                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex, "Exception nedeniyle proses çalışamadı. Bir saat sonra tekrar denenecek.");
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
            return isValid;
        }

        public bool FolderAlarmControl()
        {
            bool isValid = true;
            try
            {
                var files = GetAllTempFiles();
                if (files.Count() == 0)
                {
                    Log(MethodBase.GetCurrentMethod().Name, "Ftp dizininde hiç dosya yok");
                    statuhandler.handler.Add(new Error { error = "Ftp dizininde hiç dosya yok", fileName = null, methodName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.FileException,code=ErrorStatusCode.Error });
                    isValid = false;
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex, "Exception nedeniyle proses çalışamadı. Bir saat sonra tekrar denenecek.");
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
            return isValid;
        }

        private void ControlToValue(string sensorfilepath)
        {
            var valueList = new List<int>();
            try
            {
                var counter = 0;
                foreach (var line in File.ReadAllLines(sensorfilepath))
                {
                    if (counter==0)
                    {
                        continue;
                    }
                    else if (line=="")
                    {
                        continue;
                    }
                    var splitline = line.Trim().Split(','); //satir Sensoradi,Value şeklinde
                    //var isValidSensorValue = int.TryParse(splitline[1], out int sensorValue);

                    //if (isValidSensorValue)
                    //    valueList.Add(sensorValue);
                    counter ++;
                }
                validationStatusList.Add(IsAllValuesZero(valueList, sensorfilepath));
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
        }

        public bool IsAllValuesZero(List<int> valueList, string sensorfilepath)
        {
            bool isValid = false;
            string newPath = sensorfilepath.Substring(TempDirectory.Length + 1);
            try
            {
                foreach (var item in valueList)
                {
                    if (item == 0)
                        continue;
                    else
                    {
                        isValid = true;
                        break;
                    }
                }
                if (!isValid)
                {
                    Log(MethodBase.GetCurrentMethod().Name, newPath + " dosyasında bulunan tüm datalar 0 değeri dönmüştür!");
                    //var detectedError= new Error { error = newPath + " dosyasında bulunan tüm datalar 0 değeri dönmüştür!\n", fileName = newPath, methodName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.FileException };
                    //if (!statuhandler.handler.Contains(detectedError))
                    //{
                    statuhandler.handler.Add(new Error { error = newPath + " dosyasında bulunan tüm datalar 0 değeri dönmüştür! - [Okunmayacak] ", fileName = newPath, methodName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Error });
                    //message.Add(newPath + " dosyasında bulunan tüm datalar 0 değeri dönmüştür!\n");
                    //}
                }
                else
                {
                    Log(MethodBase.GetCurrentMethod().Name, newPath + " dosyasındaki datalar doğru biçimde gelmiştir.");
                   
                        var errorStr = newPath + " dosyadaki veriler doğru, okunmak için sıraya alındı.";
                        Error detectedError = new Error { error = errorStr, fileName = newPath, typeOfError = ErrorTypes.FileException, code = ErrorStatusCode.Success };
                        if (!statuhandler.handler.Contains(detectedError))
                        {
                            statuhandler.handler.Add(detectedError);
                            statuhandler.successedPaths.Add(newPath);
                        }
                   
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
            return isValid;
        }

        private void DownloadAllControlFiles()
        {
            var pathDataList = new List<KeyValuePair<DateTime, string>>();
            List<DateTime> lastModified = new List<DateTime>();
            PathDateList = new List<KeyValuePair<DateTime, string>>();
            if (!Directory.Exists(TempDirectory))
            {
                Directory.CreateDirectory(TempDirectory);
            }
            try
            {
                using (var sftp = new SftpClient(Host, UserName, Password))
                {
                    sftp.Connect();
                    var files = sftp.ListDirectory(@RemoteDirectory);

                    foreach (var file in files)
                    {
                        if (!file.IsDirectory && !file.IsSymbolicLink)
                        {
                            string localfilename = Path.Combine(TempDirectory, Path.GetFileName(file.Name));
                            string remotefilename = Path.Combine(RemoteDirectory, Path.GetFileName(file.Name)).Replace("\\", "//");
                            using (Stream stream = File.OpenWrite(localfilename))
                            {
                                sftp.DownloadFile(remotefilename, stream);
                            }
                            var pathDate = new KeyValuePair<DateTime, string>(file.LastWriteTime, file.Name);
                            pathDataList.Add(pathDate);
                        }
                    }
                    PathDateList.AddRange(pathDataList);
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
        }

        private void EmptyTempControlDirectory()
        {
            try
            {
                foreach (var f in Directory.GetFiles(TempDirectory))
                    File.Delete(f);
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
                statuhandler.handler.Add(new Error { error = ex.InnerException.ToString(), fileName = MethodBase.GetCurrentMethod().Name, typeOfError = ErrorTypes.InlineException, code = ErrorStatusCode.Error });
            }
        }
    }
}