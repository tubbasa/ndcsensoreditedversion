﻿using ExagateVirtualSensors;
using ExagateVirtualSensors.Models;
using Microsoft.Exchange.WebServices.Data;
using PowerPackDatabase;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.ServiceProcess;
using System.Timers;

namespace NDCVirtualSensorService
{
    public partial class NDCVirtualSensorService : ServiceBase, ILogger
    {
        public string Host { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RemoteDirectory { get; set; }
        public string LocalDirectory { get; set; }
        public string TempDirectory { get; set; }
        public string[] FileTypes { get; set; }
        public int ReadInterval { get; set; }
        public string SensorLocationFilePath { get; set; }
        public string[] MailAdresses { get; set; }

        private Timer SensorReadTimer;
        private DateTime? LastReadTime;
        private static bool IsWorkingOnIt = false;

        public NDCVirtualSensorService()
        {
            InitializeComponent();
            LastReadTime = null;
            SensorReadTimer = new Timer();
            SensorReadTimer.Interval = 600;
            SensorReadTimer.AutoReset = true;
            SensorReadTimer.Enabled = true;
            SensorReadTimer.Elapsed += SensorReadTimer_Elapsed;
        }

        public void OnDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            Host = ConfigurationManager.AppSettings["sFTP-Host"];
            UserName = ConfigurationManager.AppSettings["sFTP-UserName"];
            Password = ConfigurationManager.AppSettings["sFTP-Password"];
            RemoteDirectory = ConfigurationManager.AppSettings["sFTP-Directory"];
            LocalDirectory = ConfigurationManager.AppSettings["LocalDirectory"];
            TempDirectory = ConfigurationManager.AppSettings["TempDirectory"];
            var strinterval = ConfigurationManager.AppSettings["ReadInterval"];
            ReadInterval = int.TryParse(strinterval, out int outinterval) == true ? outinterval : 60;
            var strFileType = ConfigurationManager.AppSettings["FileTypes"];
            FileTypes = strFileType.Trim().Split(',');
            SensorLocationFilePath = ConfigurationManager.AppSettings["SensorLocationFilePath"];

            var mailAdresses = ConfigurationManager.AppSettings["MailAdresses"];
            MailAdresses = mailAdresses.Trim().Split(',');

            if (!EventLog.SourceExists("NDCVirtualSensorService"))
            {
                EventLog.CreateEventSource("NDCVirtualSensorService", "Application");
                //VirtualSensorHandler.Logger.Source = "NDCVirtualSensorService";
            }


        }

        /// <summary>
        /// Okuma süresi kadar süre geçip geçmediğini kontrol et. True dönerse yeni bir okuma yapılacak.
        /// </summary>
        /// <returns></returns>
        private bool IsItTimeToRead()
        {
            if (LastReadTime == null)
                return true;

            if (ReadInterval == 60) //saatlik okuma isteniyorsa her saatin 5. dakikasinda okuma yapılsın
            {
                if (DateTime.Now.Minute >= 10 && DateTime.Now.Minute <= 20 //Hata oluşursa 10 dakika boyunca her elapse durumunda tekrar dene.
                    && (DateTime.Now - LastReadTime.Value).TotalMinutes > 15) //Hata oluşmazsa iki okuma arasının 15 dakikadan kısa olmamasını sağla
                    return true;
                else
                    return false;
            }
            else
            {
                if ((DateTime.Now - LastReadTime.Value).TotalMinutes >= ReadInterval) //saatlik okuma istenmiyorsa interval süresi kadar geçtiği anda tekrar okuma yapılssın.
                    return true;
                else
                    return false;
            }
        }

        /// <summary>
        /// Her x saniyede bir girilecek timer metodu. Burada çalışma vakti gelip gelmediği kontrol edilir, geldiyse işlem yapılır.
        /// </summary>
        private void SensorReadTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var sensorHandler = new VirtualSensorHandler(Host, UserName, Password, RemoteDirectory, LocalDirectory, FileTypes, SensorLocationFilePath, TempDirectory);
            //sensorHandler.HasNoAlarm();
            if (IsItTimeToRead() && !IsWorkingOnIt)
            {
                IsWorkingOnIt = true;

                Log(MethodBase.GetCurrentMethod().Name, "Sensör okuma işlemi başladı.");

                WriteToDb();
                LastReadTime = DateTime.Now;
                Log(MethodBase.GetCurrentMethod().Name, "Sensör okuma işlemi tamamlandı.");
                var statuHandler = sensorHandler.HasNoAlarm();
                if (statuHandler.handler.Count > 0)
                {
                    SendMail(statuHandler);
                }

            }
        }

        /// <summary>
        /// Bir VirtualSensorHandler yaratıp sensör datalarını çeker.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<VirtualSensor> GetSensors()
        {
            var SensorHandler = new VirtualSensorHandler(Host, UserName, Password, RemoteDirectory, LocalDirectory, FileTypes, SensorLocationFilePath, TempDirectory);
            var alarms = SensorHandler.HasNoAlarm();
            var sensorValues = SensorHandler.GetSensorsWithValues();
            return sensorValues;
        }

        /// <summary>
        /// Çekilen dataları veritabanına yazar. Ardından dataları günceller.
        /// </summary>
        private void WriteToDb()
        {
            try
            {
                var sensors = GetSensors();
                var connectionString = ConfigurationManager.ConnectionStrings["DatabaseEntities"].ConnectionString;
                using (var db = new DatabaseEntities(connectionString))
                {
                    foreach (var sensor in sensors)
                    {
                        try
                        {
                            if (db.tags.Count(x => x.tagName == sensor.SensorName) == 0)
                            {
                                var t = new tags
                                {
                                    isArchive = true,
                                    isActiveTag = true,
                                    tagProtocolENUM = 0,
                                    isAlarmEnabled = false,
                                    readInterval = 0,
                                    multiplier = 1,
                                    dataTypeID = 106,
                                    tagValueTypeID = 2,
                                    isUpdateViaSnmpTrap = true,
                                    isSnmpWritable = false,
                                    isAlarmByValue = false,
                                    isInWorkTimeAlarm = true,
                                    isOutWorkTimeAlarm = true,
                                    alarmLevel = 1,
                                    dramaticallyIncreaseOrDecrease = 0,
                                    dramaticallyIncreaseCheck = false,
                                    dramaticallyDecreaseCheck = false,
                                    dramaticallyCheckENUM = 0,
                                    locationID = sensor.LocationId,
                                    tagName = sensor.SensorName,
                                    unit = "C",
                                    xCoordinate = -1,
                                    yCoordinate = -1
                                };
                               
                                db.tags.AddObject(t);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(MethodBase.GetCurrentMethod().Name, ex, sensor.SensorName + " sensörü eklenirken hata oluştu. Foreach içinde devam ediyor...");
                            continue;
                        }
                    }

                    db.SaveChanges();

                    foreach (var sensor in sensors)
                    {
                        try
                        {
                            var state = db.tagStates.FirstOrDefault(x => x.tags.tagName == sensor.SensorName);
                            if (state != null)
                            {
                                state.valueDouble = (float)sensor.Value;
                                state.date = DateTime.Now;
                            }
                        }
                        catch (Exception ex)
                        {
                            Log(MethodBase.GetCurrentMethod().Name, ex, sensor.SensorName + " sensörünün state'i güncellenirken hata oluştu. Foreach içinde devam ediyor...");
                            continue;
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex);
            }
        }

        protected override void OnStop()
        {
        }

        /// <summary>
        /// Log metodu. Yapılmak istenilen bilgilendirmeler buradan yapılır. Loglama Warning ya da Error değil Info seviyesindedir.
        /// </summary>
        public void Log(string methodname, string message)
        {
            VirtualSensorHandler.Logger.Source = "NDCVirtualSensorservice";
            VirtualSensorHandler.Logger.WriteEntry("MethodName: " + methodname + Environment.NewLine
                + "Message: " + message, EventLogEntryType.Information);
        }

        /// <summary>
        /// Log metodu. Kodda meydana gelen exceptionlar burada loglanır. Loglama Error seviyesindedir.
        /// </summary>
        public void Log(string methodname, Exception ex, string message = "")
        {
            var st = new StackTrace(ex, true);
            var frame = st.GetFrame(st.FrameCount - 1);
            var linenumber = frame.GetFileLineNumber();

            VirtualSensorHandler.Logger.Source = "NDCVirtualSensorservice";
            VirtualSensorHandler.Logger.WriteEntry("MethodName: " + methodname + " LineNumber: " + linenumber.ToString() + Environment.NewLine
                + "Exception: -> " + ex.Message + Environment.NewLine
                + "InnerExc.: -> " + (ex.InnerException ?? new Exception()).Message + Environment.NewLine
                + "Message:   -> " + message
                , EventLogEntryType.Error);
        }

        private static bool RedirectionUrlValidationCallback(String redirectionUrl)
        {
            return true;
        }

        public void SendMail(StatuHandler handler)
        {
            var test = handler.handler.Distinct();
            string MailText = string.Empty;
            MailText += "Sensör programında alınan hatalar: \r\n ";
            foreach (var item in handler.handler.Where(x => x.code == ErrorStatusCode.Error))
            {
              
                if (!MailText.Contains(item.error))
                {
                    MailText += "-"+ item.error + " \r\n ";
                }

            }
            foreach (var item in handler.handler.Where(x => x.code == ErrorStatusCode.Success))
            {
                if (!MailText.Contains(item.error)&&!handler.erroredPaths.Contains(item.fileName))
                {
                    MailText += "-" + item.error + " \r\n ";
                }
            }
            try
            {
                if (ServiceConfig.IsSmtp)
                {
                    MailMessage mail = new MailMessage
                    {
                        Subject = "Sanal Sensör Alarm Bilgilendirme",
                        From = new MailAddress(ServiceConfig.SenderAddress),
                        Body = MailText,
                        DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
                    };
                    //587
                    var smtpclient = new System.Net.Mail.SmtpClient(ServiceConfig.Domain, ServiceConfig.SmtpPort.GetValueOrDefault(587))
                    {
                        EnableSsl = ServiceConfig.Ssl,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(ServiceConfig.UserName, ServiceConfig.Password)
                    };
                    Log(MethodBase.GetCurrentMethod().Name, "Mail gönderme işlemi başladı.");
                    var mailAdresses = ConfigurationManager.AppSettings["MailAdresses"];
                    MailAdresses = mailAdresses.Trim().Split(',');
                    foreach (var t in MailAdresses)
                    {
                        mail.To.Add(t);
                    }
                    //mail.To.Add("melekpolat391@gmail.com");
                    smtpclient.Send(mail);
                    mail.Dispose();
                    Log(MethodBase.GetCurrentMethod().Name, "Mail gönderme işlemi tamamlandı.");
                }
                else
                {
                    ExchangeService service = new ExchangeService((ExchangeVersion)3)
                    {
                        Credentials = new WebCredentials(ServiceConfig.UserName, ServiceConfig.Password, ServiceConfig.Domain)
                    };

                    service.AutodiscoverUrl(ServiceConfig.SenderAddress, RedirectionUrlValidationCallback);

                    EmailMessage message = new EmailMessage(service)
                    {
                        Subject = "Sanal Sensör Alarm Bilgilendirme",
                        Body = MailText,
                    };
                    message.Send();
                }
            }
            catch (Exception ex)
            {
                Log(MethodBase.GetCurrentMethod().Name, ex, "Exception nedeniyle proses çalışamadı. Bir saat sonra tekrar denenecek.");
            }
            Console.WriteLine("Gönderildi" + Environment.NewLine);
        }
    }

    public static class ServiceConfig
    {
        public static string SenderAddress => ConfigurationManager.AppSettings["SenderAddress"];
        public static string Domain => ConfigurationManager.AppSettings["Domain"];
        public static int? SmtpPort => Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
        public static string Password => ConfigurationManager.AppSettings["Password"];
        public static string Path => ConfigurationManager.AppSettings["ReportDirectory"];
        public static bool IsSmtp => Convert.ToBoolean(ConfigurationManager.AppSettings["isSmtp"]);
        public static bool Ssl => Convert.ToBoolean(ConfigurationManager.AppSettings["isSSLEnabled"]);
        public static int ExchangeVersionEnum => Convert.ToInt32(ConfigurationManager.AppSettings["ExchangeVersionEnum"]);
        public static string UserName = ConfigurationManager.AppSettings["UserName"];
    }
}