﻿namespace NDCVirtualSensorService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NDCVirtualSensorService = new System.ServiceProcess.ServiceProcessInstaller();
            this.NDCVirtualSensorServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // NDCVirtualSensorService
            // 
            this.NDCVirtualSensorService.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.NDCVirtualSensorService.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NDCVirtualSensorServiceInstaller});
            this.NDCVirtualSensorService.Password = null;
            this.NDCVirtualSensorService.Username = null;
            // 
            // NDCVirtualSensorServiceInstaller
            // 
            this.NDCVirtualSensorServiceInstaller.DelayedAutoStart = true;
            this.NDCVirtualSensorServiceInstaller.DisplayName = "NDCVirtualSensorService";
            this.NDCVirtualSensorServiceInstaller.ServiceName = "NDCVirtualSensorService";
            this.NDCVirtualSensorServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.NDCVirtualSensorService});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller NDCVirtualSensorService;
        private System.ServiceProcess.ServiceInstaller NDCVirtualSensorServiceInstaller;
    }
}