﻿using ExagateVirtualSensors;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtualSensorTestApp
{
    class Program
    {
        public static string Host { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }
        public static string RemoteDirectory { get; set; }
        public static string LocalDirectory { get; set; }
        public static string TempDirectory { get; set; }
        public static string[] FileTypes { get; set; }
        public int ReadInterval { get; set; }
        public static string SensorLocationFilePath { get; set; }

        DateTime LastReadTime;
        static void Main(string[] args)
        {
            OnStart();
            ReadSensors();
        }
        public static void OnStart()
        {
            Host = ConfigurationManager.AppSettings["sFTP-Host"];
            UserName = ConfigurationManager.AppSettings["sFTP-UserName"];
            Password = ConfigurationManager.AppSettings["sFTP-Password"];
            RemoteDirectory = ConfigurationManager.AppSettings["sFTP-Directory"];
            LocalDirectory = ConfigurationManager.AppSettings["LocalDirectory"];
            TempDirectory = ConfigurationManager.AppSettings["TempDirectory"];
            var strFileType = ConfigurationManager.AppSettings["FileTypes"];
            FileTypes = strFileType.Split(',');
            SensorLocationFilePath = ConfigurationManager.AppSettings["SensorLocationFilePath"];
        }

        private static void ReadSensors()
        {
            try
            {
                var SensorHandler = new VirtualSensorHandler(Host, UserName, Password, RemoteDirectory, LocalDirectory, FileTypes, SensorLocationFilePath, TempDirectory);
                var sensorValues = SensorHandler.GetSensorsWithValues();
                foreach (var sens in sensorValues)
                {
                    File.AppendAllText(@LocalDirectory + @"\\sens.txt", sens.SensorName + ", " + sens.Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(Environment.NewLine + ex.Message + " -> " + Environment.NewLine + ex.InnerException ?? "");
                Console.ReadLine();
            }
        }
    }
}
